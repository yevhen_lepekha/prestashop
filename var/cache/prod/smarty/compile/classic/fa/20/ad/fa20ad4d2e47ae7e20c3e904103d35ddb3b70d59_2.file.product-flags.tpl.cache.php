<?php
/* Smarty version 3.1.39, created on 2021-08-31 14:35:47
  from '/Users/evgeniyle/Projects/prestashop/themes/classic/templates/catalog/_partials/product-flags.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_612e14137687d5_68206860',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa20ad4d2e47ae7e20c3e904103d35ddb3b70d59' => 
    array (
      0 => '/Users/evgeniyle/Projects/prestashop/themes/classic/templates/catalog/_partials/product-flags.tpl',
      1 => 1630150732,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_612e14137687d5_68206860 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '1280024741612e1413764ef7_99332725';
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_329224426612e1413767032_88393065', 'product_flags');
?>

<?php }
/* {block 'product_flags'} */
class Block_329224426612e1413767032_88393065 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_flags' => 
  array (
    0 => 'Block_329224426612e1413767032_88393065',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <ul class="product-flags">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product']->value['flags'], 'flag');
$_smarty_tpl->tpl_vars['flag']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['flag']->value) {
$_smarty_tpl->tpl_vars['flag']->do_else = false;
?>
            <li class="product-flag <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['label'], ENT_QUOTES, 'UTF-8');?>
</li>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>
<?php
}
}
/* {/block 'product_flags'} */
}
