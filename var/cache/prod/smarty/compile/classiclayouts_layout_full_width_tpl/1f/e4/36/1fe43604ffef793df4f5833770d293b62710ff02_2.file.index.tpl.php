<?php
/* Smarty version 3.1.39, created on 2021-08-31 14:35:47
  from '/Users/evgeniyle/Projects/prestashop/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_612e14137dce01_20958992',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1fe43604ffef793df4f5833770d293b62710ff02' => 
    array (
      0 => '/Users/evgeniyle/Projects/prestashop/themes/classic/templates/index.tpl',
      1 => 1630150732,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_612e14137dce01_20958992 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1285127418612e14137db636_88936499', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_979711662612e14137db996_75055203 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_1973330001612e14137dc163_79714946 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_1219060213612e14137dbeb7_18352277 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1973330001612e14137dc163_79714946', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_1285127418612e14137db636_88936499 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_1285127418612e14137db636_88936499',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_979711662612e14137db996_75055203',
  ),
  'page_content' => 
  array (
    0 => 'Block_1219060213612e14137dbeb7_18352277',
  ),
  'hook_home' => 
  array (
    0 => 'Block_1973330001612e14137dc163_79714946',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_979711662612e14137db996_75055203', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1219060213612e14137dbeb7_18352277', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
