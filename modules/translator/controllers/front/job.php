<?php

// link to this file is available: http://<hostname>/module/translator/job

class TranslatorJobModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        $lang = Configuration::get('TRANSLATOR_TARGET_LANG');
        $data = Db::getInstance()->execute('
            SELECT `description`, `description_short`, `name` 
            FROM `' . _DB_PREFIX_ . 'product_lang` 
            WHERE id_lang = ' . (int) $lang . ' AND is_translated = NULL
            ORDER BY id_product DESC 
            LIMIT 10
        ');

        // в цикле по data отправляем реквесты к Translation API
        $response = [];

        Db::getInstance()->execute('
            UPDATE `' . _DB_PREFIX_ . 'product_lang` 
            SET `description` =  \'' . $response['description'] . '\', 
                `description_short` = \'' . $response['description_short'] . '\', 
                `name` = \'' . $response['name'] . '\' 
            WHERE  `id_product` = ' . (int) $data['id_product'] . ';
        ');
        
        exit();
    }
}