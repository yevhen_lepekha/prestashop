<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class Translator extends Module
{
    public function __construct()
    {
        $this->name = 'translator';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Yevhen Lepekha';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => '1.7.99'
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Translator');
        $this->description = $this->l('Translate description of your products into any language.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('TRANSLATOR')) {
            $this->warning = $this->l('No name provided.');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        Db::getInstance()->execute('
            ALTER TABLE `' . _DB_PREFIX_.'product_lang` 
            ADD is_translated int(1) unsigned DEFAULT NULL
        ');

        return (
            parent::install() && Configuration::updateValue('TRANSLATOR', 'Translator')
        ); 
    }

    public function uninstall()
    {
        Db::getInstance()->execute('
            ALTER TABLE `' . _DB_PREFIX_.'product_lang` 
            DROP COLUMN is_translated
        ');

        return (
            parent::uninstall() 
                && Configuration::deleteByName('TRANSLATOR')
                && Configuration::deleteByName('TRANSLATOR_TARGET_LANG')
                && Configuration::deleteByName('TRANSLATOR_API_KEY')
        );
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitTranslator')) {
            $values = [
                'target_lang' => Tools::getValue('target_lang'),
                'api_key' => Tools::getValue('api_key')
            ];

            Configuration::updateValue('TRANSLATOR_TARGET_LANG', $values['target_lang']);
            Configuration::updateValue('TRANSLATOR_API_KEY', $values['api_key']);

            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }

        return '';
    }

    public function renderForm()
    {
        $languages = Language::getLanguages(false);
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $options = array_filter($languages, fn($value) => (int) $value['id_lang'] !== $default_lang);
        $formatted_options = array_map(fn($value) => ['id' => $value['iso_code'], 'name' => $value['name']], $options);
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'select',
                        'label' => $this->l('Choose target language:'),
                        'desc' => $this->l('It is needed to have at least one non-default language'),
                        'name' => 'target_lang',
                        'required' => true,
                        'options' => [
                            'query' => $formatted_options,
                            'id' => 'id',
                            'name' => 'name'
                        ]
                    ],
                    [
                        'type' => 'text',
                        'class'    => 'lg',
                        'label' => $this->l('API key:'),
                        'name' => 'api_key',
                        'required' => true,
                        'desc' => $this->l('Enter the API key associated to your Google Cloud account')
                    ]
                ],
                'submit' => [
                    'title' => $this->l('Save')
                ]
            ],
        ];

        $helper = new HelperForm();
        $helper->table = $this->table;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&' . http_build_query(['configure' => $this->name]);
        $helper->submit_action = 'submit' . ucfirst($this->name);
        $helper->default_form_language = (int) Configuration::get('PS_LANG_DEFAULT');
        $helper->module = $this;
        $helper->fields_value['target_lang'] = Tools::getValue('target_lang', Configuration::get('TRANSLATOR_TARGET_LANG'));
        $helper->fields_value['api_key'] = Tools::getValue('api_key', Configuration::get('TRANSLATOR_API_KEY'));

        return $helper->generateForm(array($fields_form));
    }
}